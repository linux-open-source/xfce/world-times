# World Times Generic Monitor

![World Times Generic Monitor](share/worldtimes.jpg "World Times Generic Monitor")

## Requirements

* xfce4 version 4.16
* xclip
* xfce4-genmon-plugin

## Files

![World Times Tree](share/worldtimes-tree.png "World Times Tree")

## Operation

Everytime the **Generic Monitor** period elapses the 'worldtimes-genmon.sh' script will be invoked.
This will occur even while the **Generic Monitor** tooltip is being displayed.

Left-clicking the **Generic Monitor** icon will invoke the 'worldtimes-clip.sh'
script to place current locale datetime information into the clipboard.

Any time changes are made to this script, execute the following command in a
terminal window;

        xfce4-panel --plugin-event=genmon-<XX>:refresh:bool:true

The '*\<XX>*' is an internal number assigned by xfce4 panel.

To find out what the internal number is on your desktop, right-click the
**Generic Monitor** icon in the panel, select <kbd>Panel</kbd>, select
<kbd>Panel Preferences...</kbd>, click <kbd>Items</kbd> tab, hover over
<kbd>Generic Monitor(external)</kbd> and the internal name will
appear in the tooltip.

## Additional references

* [xfce4-genmon-plugin doc](https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin/start)

* [Pango markup doc](https://developer.gnome.org/pygtk/stable/pango-markup-language.html)

* xfce4-genmon-plugin config file

  ${HOME}.config/xfce4/panel/genmon-\<XX>.rc

* List timezones

        timedatectl list-timezones

* Basic timezone information

  * tzdata package
  * zdump command
  * /usr/share/zoneinfo directory<p />

* Clear the clipboard

        xclip -selection clip </dev/null

* Alternative globes

  * /usr/share/icons/Papirus/32x32/apps/clockify.svg
  * /usr/share/icons/ePapirus/24x24/actions/globe.svg<p />

* List fonts

        fc-list
