#!/bin/bash
#===============================================================================
#
# worldtimes-genmon.sh
# Copyright (C) 2021-2024 RusSte, Inc.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or any later
# version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
# Package:  world-times
# Version:  2.0
# Author:   RusSte, Inc. (russte.software@gmail.com)
# License:  GPLv2
#
# A 'Generic Monitor' script to display select world times, and with a
# left-click, place the current time information in the Xorg clipboard.
#
# When you hover over the 'Generic Monitor' using 'worldtimes-genmon.sh',
# the dates and times will be displayed in a tooltip for the locales.
#
# The locales are located in config/worldtimes.shl.
#
# Weather information for each locale may be displayed as well.  See the
# 'weather' variables in the 'config/worldtimes.shl' file.
#
#===============================================================================

set -u

# ------------------------------------------------------------------------------
#

# Location of 'worldtimes' scripts/image
#
declare -- worldTimesHome="${0%/lib/*}"

# Script to be executed when 'Generic Monitor' panel icon is left-clicked.
#
declare -- clickScript="${worldTimesHome}/lib/worldtimes-clip.sh"

# Icon to be display for 'Generic Monitor' panel item
#
declare -- icon="${worldTimesHome}/share/worldtimes-globe.svg"

# Configuration file containing the 'locales' variable
#
declare -- configFile="${worldTimesHome}/config/worldtimes.shl"

# Location of timestamp files used for timing of updates
#
declare -- timestampsDir="/tmp/${USER}/worldtimes-genmon"
declare -- periodEventFile="${timestampsDir}/periodEvent"
declare -- periodFile="${timestampsDir}/period"
declare -- weatherFile="${timestampsDir}/weather"
declare -- wttrFile="${timestampsDir}/wttr"

if [[ ! -d "${timestampsDir}" ]]; then
  mkdir --parents --verbose "${timestampsDir}"
fi

# The current 'genmon' 'period'.
#
declare -- period=0

# Load configuration for 'locales'
#
source "${configFile}"

if (( weatherInterval == 0 )); then
  declare -- weather=false
else
  declare -- weather=true
fi

# Write 'stdin' to 'logFile'
#
function logger() {
  if [[ -n "${logFile}" ]]; then
    local -- logFileSize="$( stat -c %s "${logFile}" )"
    #echo "$( date --iso-8601=ns ): size [${logFileSize}]: limit [${logFileMaxSize}]" >>"${logFile}"

    if (( logFileSize > logFileMaxSize )); then
      echo "$( date --iso-8601=ns ): cleared"  >"${logFile}"
    fi

    while IFS=$'\n' read -r; do
      echo "${REPLY}" >>"${logFile}"
    done
  fi
}

# Print selected world times and highlight (bold) the current timezone entry
#
#   args      boolean     if 'true', get weather
#
function print_worldtimes() {
  if (( ${#} != 1 )); then
    return 2
  fi

  printf -- '<tool><span font="'"${fontDescription}"'">'

  local -- pw_GetWeather="${1?if 'true', get weather}"

  # Print selected world times sorted by ascending julian date and time
  #
  function process_locales() {
    local -- locale=''
    local -- localeDateTime=''

    # 'locales' array is defined in the 'worldtimes.shl' configuration
    #
    for locale in "${locales[@]}"; do
      localeDateTime="$( TZ="${locale}" date '+%Y%j%H %a, %b %d, %Y %H:%M' )"

      # Output: America/Chicago 202111303 Fri, Apr 23, 2021 03:55
      #
      printf -- '%s %s\n' "${locale}" "${localeDateTime}"
    done |
    sort --key=2n
  } # end process_locales

  # Tooltip header
  #
  if ${weather}; then
    printf -- '<span weight="bold">      Current Time         Weather</span>\n'
  else
    printf -- '<span weight="bold">      Current Time</span>\n'
  fi

  local -- currentTZ=''
  local -- dateHeader=''
  local -- julianDateKey=''
  local -- worldDateTime=''
  local -- worldTZ=''

  # Isolate the current timezone from 'timedatectl show' output
  #
  read -r currentTZ < <( timedatectl show )
  currentTZ="${currentTZ/Timezone=/}"

  echo "$( date --iso-8601=ns ): started" > >( logger )

  local -- configTime="$( stat -c %Y "${configFile}" )"

  if [[ -e "${wttrFile}" ]]; then
    local -- wttrTime="$( stat -c %Y "${wttrFile}" )"

    if (( configTime > (wttrTime - 1) )); then
      declare -A wttrs=()
      declare -p wttrs >"${wttrFile}"
      pw_GetWeather=true
    else
      source "${wttrFile}"
    fi
  else
    declare -A wttrs=()
    declare -p wttrs >"${wttrFile}"
    pw_GetWeather=true
  fi

  # Generate each selected world timezone's name and time
  # making the entry matching the current timezone bold.
  #
  # The entries will be shown in ascending order by their
  # respective julian date and time.
  #
  while read -re worldTZ julianDateKey worldDateTime; do
    if ${weather}; then
      if ${pw_GetWeather}; then
        local -- wttr="$(curl "wttr.in/${worldTZ##*/}?format=3&${weatherUnits}" )"
        wttr="${wttr##*:}"
        wttrs["${worldTZ##*/}"]="${wttr}"
      else
        wttr="${wttrs["${worldTZ##*/}"]}"
      fi
    fi

    if [[ "${dateHeader}" != "${julianDateKey:0:7}" ]]; then
      dateHeader="${julianDateKey:0:7}"
      printf -- '\n<span weight="bold">     %s</span>\n' "${worldDateTime% *}"
    fi

    if [[ "${worldTZ}" == "${currentTZ}" ]]; then
      printf -- '<span weight="bold">%-20s</span> %s' \
        "${worldTZ}" "${worldDateTime##* }"
    else
      printf -- '%-20s %s' "${worldTZ}" "${worldDateTime##* }"
    fi

    if ${weather}; then
      read -r icon temp <<<"${wttr}"
      printf -- ' %6s %s \n' "${temp}" "${icon}"
    else
      printf -- '\n'
    fi
  done < <( process_locales )

  printf -- '</span></tool>'

  #declare -p wttrs > >( logger )
  declare -p wttrs >"${wttrFile}"

  echo "$( date --iso-8601=ns ): ended" > >( logger )
} # end print_worldtimes

# 'Generic Monitor' reads stdout from this script and parses it for specific tags
#
# See: https://docs.xfce.org/panel-plugins/xfce4-genmon-plugin/start
#

# <css>Genmon gtk3 css formatting</css>
#
# See: https://gitlab.xfce.org/panel-plugins/xfce4-genmon-plugin/
#       -/blob/master/CSS%20Styling.txt
#
# Note: The format of the css is dependent on the version of gtk3.
#       On Manjaro use 'pacman -Qi gtk3' to find the version of gtk3 installed.
#
printf -- \
'<css>'\
'.genmon_imagebutton {opacity: 0.5; padding-left: 2px; padding-top: 2px;}'\
'</css>'

# <img>Path to the image to display</img>
#
# 'Generic Monitor' xfce4-panel plugin icon
#
printf -- '<img>%s</img>' "${icon}"

# <click>The command to be executed when clicking on the image</click>
#
# 'Generic Monitor' panel icon left-click places date in clipboard (XA_CLIPBOARD)
#
printf -- '<click>%s</click>' "${clickScript}"

# <tool>Tooltip text</tool>
#
# 'Generic Monitor' panel icon hover tooltip shows selected global timezones' current times
#
# The tooltop text will dynamically update using the 'Generic Monitor' 'Period' value.
#
# This update will even occur if the tooltip is already being displayed.
#

# Calculate current 'genmon' 'period'
#
#   args      none
#
#   returns   integer     0 - always
#
function calculatePeriod() {
  local -- rc=0

  local -- thisPeriodEvent="$( date +%s )"
  local -- lastPeriodEvent=0

  if [[ ! -e "${periodEventFile}" ]]; then
    echo '0' >"${periodFile}"
    echo "${thisPeriodEvent}" >"${periodEventFile}"

    lastPeriodEvent=${thisPeriodEvent}
  else
    read -r lastPeriodEvent <"${periodEventFile}"
  fi

  if (( (thisPeriodEvent - lastPeriodEvent) != 0 )); then
    echo $(( (thisPeriodEvent - lastPeriodEvent) )) >"${periodFile}"
    read -r period <"${periodFile}"
  fi

  echo "${thisPeriodEvent}" >"${periodEventFile}"

  return ${rc}
}

calculatePeriod

# Check for 'weather' update
#
#   args      none
#
#   returns   integer     0 - update 'weather'
#                         1 - do not update 'weather'
#
function updateWeather() {
  local -- rc=1

  if ${weather}; then
    local -- weatherCountdown=0

    if [[ ! -e "${weatherFile}" ]]; then
      echo 'update' > >( logger )
      echo "${weatherInterval}" >"${weatherFile}"
    fi

    read -r weatherCountdown <"${weatherFile}"

    weatherCountdown=$(( weatherCountdown - period ))

    if (( weatherCountdown < 1 )); then
      (( weatherCountdown=weatherInterval ))
      rc=0
    fi

    echo "${weatherCountdown}" >"${weatherFile}"
  fi

  return ${rc}
}

if updateWeather; then
  echo print_worldtimes true > >( logger )
  declare -- text="$( print_worldtimes true )"
else
  echo print_worldtimes false > >( logger )
  declare -- text="$( print_worldtimes false )"
fi

echo "${text}" > >( logger )
echo "${text}"
