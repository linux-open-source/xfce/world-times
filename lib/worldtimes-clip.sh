#!/bin/bash
#===============================================================================
#
# worldtimes-clip.sh
# Copyright (C) 2021-2024 RusSte, Inc.
#
# This program is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by the
# Free Software Foundation; either version 2 of the License, or any later
# version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
# for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
#
# Package:  world-times
# Version:  2.0
# Author:   RusSte, Inc. (russte.software@gmail.com)
# License:  GPLv2
#
# A 'Generic Monitor' script to place the current time information in the
# clipboard.
#
# When you left-click the 'Generic Monitor' using 'worldtimes-genmon.sh',
# the current locale datetime will be placed in the Xorg clipboard.
# Use ctrl-v to paste.
#
# Change the format in config/worldtimes.shl as desired.
#
# See 'man date' for formatting options.
#
#===============================================================================

# ------------------------------------------------------------------------------
#

# Location of 'worldtimes' scripts/image
#
declare -- worldTimesHome="${0%/lib/*}"

# Configuration file containing the 'clipFormat' variable
#
declare -- config="${worldTimesHome}/config/worldtimes.shl"

# Load configuration
#
source "${config}"

declare -a dateParams=()

# 'clipFormat' is defined in the 'worldtimes.shl' configuration
#
dateParams=( "${clipFormat}" )

xclip -selection clip <<<"$( date "${dateParams[@]}" )"
